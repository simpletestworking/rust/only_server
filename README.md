# A rust project for test a web application.
## project features
- manage compile
- manage basic web server

## how to execute
- enter to directory

- get info for rust environment
```
rustup show
```
- get info for the rust compiler
```
rustc --version
```
- update the all environment
```
rustup update
```
- complie and run the web app
```
cargo run
```
